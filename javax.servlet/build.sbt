ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.13"

lazy val root = (project in file("."))
  .settings(
    name := "javax.servlet"
  )

name := "ServletSample"

artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
  artifact.name + "." + artifact.extension
}

libraryDependencies ++= Seq(
  "javax.servlet" % "javax.servlet-api" % "4.0.1" % "compile",
  "org.mortbay.jetty" % "jetty" % "6.1.22" % "container",
  "org.owasp.encoder" % "encoder" % "1.2.3"
)

enablePlugins(JettyPlugin)

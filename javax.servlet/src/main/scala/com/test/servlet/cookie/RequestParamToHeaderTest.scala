package com.test.servlet.cookie

import org.owasp.encoder.Encode
import java.io.IOException
import java.net.URLEncoder
import java.nio.charset.StandardCharsets
import java.util
import javax.servlet.ServletException
import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse}

// Try "Test\r\ning" as an input parameter if required (http://localhost:8080/servletsample/RequestParamToHeaderTest?input=Test\r\ning&type=a)
// Case 1: http://localhost:8080/servletsample/RequestParamToHeaderTest?input=safe&type=a
// Case 2: http://localhost:8080/servletsample/RequestParamToHeaderTest?input=safe&type=b
// Case 3: http://localhost:8080/servletsample/RequestParamToHeaderTest?input=safe&type=c
// Case 4: http://localhost:8080/servletsample/RequestParamToHeaderTest?input=safe&type=d
// Case 5: http://localhost:8080/servletsample/RequestParamToHeaderTest

class RequestParamToHeaderTest extends HttpServlet {

  @throws[ServletException]
  @throws[IOException]
  override def doGet(req: HttpServletRequest, resp: HttpServletResponse): Unit = {
    try {
      val scenario = req.getParameter("type")
      var headerValue = ""
      scenario match {
        case "a" =>
          headerValue = getParameterUsage(req)
          // ruleid: scala_cookie_rule-RequestParamToHeader
          resp.addHeader("ADD_HEADER", headerValue)


        case "b" =>
          headerValue = getParameterNamesUsage(req)
          // ruleid: scala_cookie_rule-RequestParamToHeader
          resp.setHeader("SET_HEADER", headerValue)


        case "c" =>
          headerValue = getParameterValuesUsage(req)
          // ruleid: scala_cookie_rule-RequestParamToHeader
          resp.addHeader("ADD_HEADER", headerValue)


        case "d" =>
          headerValue = getParameterMapUsage(req)
          // ruleid: scala_cookie_rule-RequestParamToHeader
          resp.setHeader("SET_HEADER", headerValue)


        case _ =>
          headerValue = getParameterUsage(req)
          val isValidHeader = headerValue.matches("^[a-zA-Z0-9.]*$")
          if(isValidHeader){
            // ok: scala_cookie_rule-RequestParamToHeader
            resp.addHeader("ADD_HEADER", headerValue)
          }

          val escapedHeaderTwo = URLEncoder.encode(headerValue, StandardCharsets.UTF_8.toString)
          // ok: scala_cookie_rule-RequestParamToHeader
          resp.addHeader("ADD_HEADER_TWO", escapedHeaderTwo)

          val escapedHeaderThree = Encode.forUriComponent(headerValue)
          // ok: scala_cookie_rule-RequestParamToHeader
          resp.addHeader("ADD_HEADER_THREE", escapedHeaderThree)

      }
    } catch {
      case e: Exception =>
        resp.addHeader("Add-From-Param-Error", e.getMessage)
    }
    try {
      val out = resp.getWriter
      try out.println("RequestParamToHeader Test: " + resp)
      finally if (out != null) out.close()
    }
  }

  def getParameterUsage(req: HttpServletRequest): String = req.getParameter("input")

  def getParameterNamesUsage(req: HttpServletRequest): String = {
    val parameterNames: util.Enumeration[String] = req.getParameterNames
    if (parameterNames.hasMoreElements) {
      val firstParamName: String = parameterNames.nextElement
      val firstParamValues: Array[String] = req.getParameterValues(firstParamName)
      return firstParamValues(0)
    }
    ""
  }

  def getParameterValuesUsage(req: HttpServletRequest): String = {
    val parameterValues: Array[String] = req.getParameterValues("input")
    if (parameterValues != null && parameterValues.length > 0) return parameterValues(0)
    ""
  }

  def getParameterMapUsage(req: HttpServletRequest): String = {
    val parameterMap: util.Map[String, Array[String]] = req.getParameterMap
    if (!parameterMap.isEmpty) {
      val firstEntry: util.Map.Entry[String, Array[String]] = parameterMap.entrySet.iterator.next
      val firstParamName: String = firstEntry.getKey
      val firstParamValues: Array[String] = firstEntry.getValue
      return firstParamValues(0)
    }
    ""
  }
}
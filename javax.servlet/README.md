## javax.servlet

Used for testing web applications source/sinks based on the older javax.servlet namespace.

### Adding tests

Simply create a new package or find the applicable package under `src/main/scala/com/gitlab/<type>` to create the new class.
Create the class that demonstrates the vulnerability.

Add the servlet to the src/main/webapp/WEB-INF/web.xml:
```
  ...
  <servlet>
	<servlet-name>TestInject</servlet-name>
	<display-name>TestInject</display-name>
	<description>Test GET, POST methods of Servlet</description>
	<servlet-class>com.test.servlet.inject.TestInject</servlet-class>
  </servlet>

  <servlet-mapping>
	<servlet-name>TestInject</servlet-name>
	<url-pattern>/TestInject</url-pattern>
  </servlet-mapping>
```

### Running

Run:
```
sbt clean
sbt compile
sbt package
docker build -t scalaservlet . && docker run --rm -p 8080:8080 --name scalaservlet scalaservlet
# Once started, hit the endpoint, all servlets are under the "/ServletSample" application namespace.
curl -vvv "http://localhost:8080/servletsample/RequestParamToHeaderTest"
```